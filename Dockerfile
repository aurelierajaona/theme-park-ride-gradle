FROM openjdk:12-alpine
VOLUME /tmp
ADD /build/libs/*.jar    theme-park-ride-gradle.jar
EXPOSE 5000
ENTRYPOINT exec java -jar theme-park-ride-gradle-0.0.1-SNAPSHOT.jar
HEALTHCHECK CMD curl --fail http://localhost:5000/ride || exit 1
